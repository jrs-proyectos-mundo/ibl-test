import "./index.sass"

import Cards from "./js/cards.js"
import Controls from "./js/controls"
import Footer from "./js/footer"
import TheMap from "./js/map"
import Panel from "./js/panel"

new Cards()
new Controls()
TheMap.buildingMap() 
new Footer()
new Panel()