import TheMap from "./map"

class PanelListeners {
  showA () {
    this.panelB.classList.remove("panel__title--active")
    this.panelA.classList.add("panel__title--active")

    this.controlsA.classList.remove("controls--hidden")
    this.controlsB.classList.add("controls--hidden")
    
    this.map.classList.add("map--hidden")
    this.cards.classList.remove("map--hidden")

    this.header.textContent = "perfil del usuario"
  }

  showB () {
    this.panelA.classList.remove("panel__title--active")
    this.panelB.classList.add("panel__title--active")
    
    this.controlsB.classList.remove("controls--hidden")
    this.controlsA.classList.add("controls--hidden")

    this.map.classList.remove("map--hidden")
    this.cards.classList.add("map--hidden")
    
    this.header.textContent = "mapa interactivo"

    TheMap.cleanMap()
  }
}

class Panel extends PanelListeners{
  panelA
  panelB

  controlsA
  controlsB

  map
  cards

  header

  constructor() {
    super()
    
    this.panelA = document.getElementById("panelA")
    this.panelA.addEventListener("click", () => this.showA())
    this.panelB = document.getElementById("panelB")
    this.panelB.addEventListener("click", () => this.showB())

    this.controlsA = document.getElementById("controlsUser")
    this.controlsB = document.getElementById("controlsMap")

    this.header = document.getElementById("header")

    this.map = document.getElementById("mapScene")
    this.cards = document.getElementById("cards")
  }
}

export default Panel