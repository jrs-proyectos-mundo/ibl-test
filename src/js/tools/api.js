class Api_REST {
    async getData() {
        let data = null

        const res = await fetch("https://randomuser.me/api/")

        if (res.ok)
            data = await res.json()
        else
            console.error("No se pudo obtener datos de la api")

        return data
    }
}

export default Api_REST