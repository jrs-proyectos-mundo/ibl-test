class FooterListeners {
  toTheRepo () {
    window.open("https://gitlab.com/jrs-proyectos-mundo/ibl-test", "_blank")
  }
}

class Footer extends FooterListeners {
  #toRepo = null

  constructor () {
    super()

    this.#toRepo = document.getElementById("toRepo")
    this.#toRepo.addEventListener("click", () => this.toTheRepo())
  }
}

export default Footer