import Api_REST from "./tools/api.js"

class TheCard {
    static detail(titleDetail, className, valueDetail) {
        const detail = document.createElement("div")
        detail.classList.add(className)
        
        const title = document.createElement("span")
        title.classList.add(`${className}__title`)
        title.textContent = titleDetail
        detail.appendChild(title)
        
        const value = document.createElement("span")
        value.classList.add(`${className}__value`)
        value.textContent = valueDetail
        detail.appendChild(value)

        return detail
    }

    static create(data) {
        const card = document.createElement("div")
        card.classList.add("cards__card")
        
        const name = document.createElement("h3")
        name.textContent = `${data.name.title} ${data.name.first} ${data.name.last}`
        name.classList.add("cards__card__name")
        card.appendChild(name)

        const imgContainer = document.createElement("div")
        imgContainer.classList.add("cards__card__image")
        card.appendChild(imgContainer)
        const img = document.createElement("img")        
        img.setAttribute("src", data.picture.large)
        img.setAttribute("attr", `Fotografia de ${data.name.title} ${data.name.first} ${data.name.last}`)
        imgContainer.appendChild(img)

        const detailsA = document.createElement("div")
        detailsA.classList.add("cards__card__detail--container")
        card.appendChild(detailsA)

        detailsA.appendChild(TheCard.detail("edad", "cards__card__detail", data.dob.age))
        detailsA.appendChild(TheCard.detail("genero", "cards__card__detail", data.gender))
        detailsA.appendChild(TheCard.detail("pais", "cards__card__detail", data.location.country))
        detailsA.appendChild(TheCard.detail("ciudad", "cards__card__detail", data.location.city))

        const detailsB = document.createElement("div")
        detailsB.classList.add("cards__card__detail--container")
        card.appendChild(detailsB)

        detailsB.appendChild(TheCard.detail("estado", "cards__card__detail", data.location.state))
        detailsB.appendChild(TheCard.detail("código postal", "cards__card__detail", data.location.postcode))
        detailsB.appendChild(TheCard.detail("email", "cards__card__detail", data.email))
        detailsB.appendChild(TheCard.detail("telefono", "cards__card__detail", data.phone))

        return card
    }
}


class Cards {
    #cards = null
    #data = null

    constructor () {
        this.buildingCards()
    }

    async buildingCards () {
        this.#cards = document.getElementById("cards")

        const API = new Api_REST()
        this.#data = await API.getData()
        
        this.#data.results.forEach(data => {
            // console.log(data)
            this.#cards.appendChild(TheCard.create(data)
)
        })
    }
}

export default Cards