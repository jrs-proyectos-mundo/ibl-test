import Cards from "./cards.js"

class ControlsListener {
  newUser () {
    document.getElementById("cards").innerHTML = ""

    new Cards()
  }
}

class Controls extends ControlsListener{
  #btnUser

  constructor () {
    super()

    this.#btnUser = document.getElementById("controlUser")

    this.#btnUser.addEventListener("click", () => this.newUser())
  }
}

export default Controls