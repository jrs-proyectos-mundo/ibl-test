class MapListeners {
  createMarker(lat, lon) {
    L.marker([lat, lon]).addTo(this.map)
  }

  generateCircle() {
    // TODO: Falta agregar los marcadores. Ya no me dio tiempo ;(
    if (this.firstMarker === null) {
      alert("Agrega 1 marcador en el mapa interactivo, haciendo click en cualquier zona del mapa ;)")
      return
    }

    let circle = null
    console.log(this.firstMarker, this.rangeCircle)

    if (this.firstMarker !== null && this.secondMarker === null) {
      circle = L.circle([this.firstMarker[0], this.firstMarker[1]], {
        color: "purple",
        fillcolor: "#f02",
        fillOpacity: 0.5,
        radius: this.rangeCircle
      }).addTo(this.map)
    } else {
      circle = L.circle([this.secondMarker[0], this.secondMarker[1]], {
        color: "purple",
        fillcolor: "#f02",
        fillOpacity: 0.5,
        radius: this.rangeCircle
      }).addTo(this.map)
    }
  }

  generateRoute() {
    if (this.firstMarker !== null && this.secondMarker !== null) {
      L.Routing.control({
        waypoints: [
          L.latLng(this.firstMarker[0], this.firstMarker[1]),
          L.latLng(this.secondMarker[0], this.secondMarker[1])
        ]
      }).addTo(this.map)
    } else {
      if (this.firstMarker !== null)
        alert("Agrega 1 marcador más en el mapa interactivo, haciendo click en cualquier zona del mapa ;)")
      else
        alert("Agrega 2 marcadores en el mapa interactivo, haciendo click en dos zonas del mapa ;)")
    }
  }

  cleanMap() {
    this.map.remove()

    this.firstMarker = null
    this.secondMarker = null

    this.buildingMap()
  }

  updateRange() {
    this.spanRange.textContent = `${this.inputRange.value} mts`
    this.rangeCircle = this.inputRange.value
  }
}

class Map extends MapListeners {
  map = null
  #token = "pk.eyJ1IjoiaGFreW4iLCJhIjoiY2wwdWs5NjRzMDJzczNtcDVndHliZ2N1MCJ9.Y7YqeVWFvNdtigYRd7ZxUg"

  currentLat = 20.5931
  currentLon = -100.392

  firstMarker = null
  secondMarker = null

  #btnRouting = null
  #btnClean = null
  #btnCircle = null

  inputRange = null
  spanRange = null
  rangeCircle = null

  constructor() {
    super()

    this.#btnRouting = document.getElementById("controlRouting")
    this.#btnRouting.addEventListener("click", () => this.generateRoute())

    this.#btnCircle = document.getElementById("controlCircle")
    this.#btnCircle.addEventListener("click", () => this.generateCircle())

    this.#btnClean = document.getElementById("cleanMap")
    this.#btnClean.addEventListener("click", () => this.cleanMap())

    this.inputRange = document.getElementById("inputRange")
    this.inputRange.addEventListener("change", () => this.updateRange())
    this.spanRange = document.getElementById("rangeCircle")
    this.spanRange.textContent = `${this.inputRange.value} mts`
    this.rangeCircle = this.inputRange.value
  }

  buildingMap() {
    this.map = L.map("mapScene").setView([this.currentLat, this.currentLon], 13)
    this.map.addEventListener("click", e => {
      if (this.firstMarker == null) {
        this.createMarker(e.latlng.lat, e.latlng.lng)
        this.firstMarker = [e.latlng.lat, e.latlng.lng]
      } else {
        if (this.secondMarker == null) {
          this.createMarker(e.latlng.lat, e.latlng.lng)
          this.secondMarker = [e.latlng.lat, e.latlng.lng]
        }
      }
    })

    L.tileLayer(`https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${this.#token}`, {
      attribution: 'Joaquin Mapa Test, Imagen Mapa © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 14,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: this.#token
    }).addTo(this.map)
  }
}


const TheMap = new Map()

export default TheMap