const path = require("path")
const fs = require("fs")

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const webpack = {
	watch: true,
	entry: path.resolve(__dirname, 'src/index.js'),
	output: {
		filename: "[name].js",
		path: path.resolve(__dirname, 'build/js')
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				// Para no transpilar los archivos JS de las carpetas que no desee se usara un array [/NOMBRE DE LA CARPETA/, /  /] en caso contrario simplemente agregar /NOMBRE DE LA CARPETA/
				exclude: [/node_modules/],
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
						plugins: [
							"@babel/plugin-transform-runtime"
						]
					}
				}
			},
			{
				test: /\.sass$/,
				use: [
					{ loader: MiniCssExtractPlugin.loader },
					// { loader: 'style-loader' }, No es necesario cuando se pretende extraer el Css, ya que este loader solo agrega los estilos en un tag que crea <style></style>
					{ loader: 'css-loader' },
					// { loader: 'postcss-loader', options: {
					//   indent: 'postcss',
					//   plugins: loader => [
					//     require('autoprefixer')(),
					//     require('cssnano')()
					//   ]
					// } },
					{ loader: 'sass-loader' },
				]
			},
			{
				test: /\.pug$/,
				use: [
					{ loader: 'simple-pug-loader' }
				]
			},
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '../css/[name].css',
			chunkFilename: '[id].css'
		}),
	]
}

webpack["plugins"].push(
	new HtmlWebpackPlugin({
		filename: path.resolve(__dirname, "build/index.html"),
		inject: 'body',
		hash: true,
		cache: false,
		template: path.resolve(__dirname, "src/index.pug"),
	})
)



module.exports = webpack